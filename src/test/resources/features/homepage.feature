@Rajini
Feature:HomePage
  As a dealer
  I want to visit QAworks HomePage
  So that I can access the site

  Scenario Outline:Home Page Loading
    Given I visited  "<URL>"
    When I see the "<aTitle>" of the Home Page
    Then I should see the "<tab>" of the Home Page
#
    Examples:

      | URL                    | aTitle                                | tab     |
      | http://www.qaworks.com | Software Quality and Delivery Experts | Contact |