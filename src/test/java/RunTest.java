import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

/**
 * Created by Rajini1 on 30/07/2017.
 */


@RunWith(Cucumber.class)

@CucumberOptions(features = {"src/test/resources"},
        format = {"pretty", "html:target/cucumber", "json:target/cucumber-report.json"},
        tags = {"@Rajini"})

public class RunTest {

        static WebDriver driver;

        @BeforeClass
        public static void start() {
                try {
                        BrowserFactory.StartBrowser("Firefox", "http://www.qaworks.com");
                        driver = BrowserFactory.driver;
                } catch (Exception e) {
                        e.printStackTrace();
                }

        }

        @AfterClass
        public static void stop() {
                driver.quit();
                driver = null;
        }
}


//        public class RunTest {
//
//        public WebDriver driver;
//
//        @Before
//        public void startBrowser()
//        {
//        driver= new ChromeDriver();
//        driver.get("http://www.qaworks.com");
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        driver.manage().window().maximize();
//        }
//        }
