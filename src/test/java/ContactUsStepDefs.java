import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by rajinim on 30/07/2017.
 */
public class ContactUsStepDefs {

    HomePage homePage;
    UtilsPage utilsPage;
    ContactUsPage contactUsPage;

    public ContactUsStepDefs() {
        homePage = new HomePage();
        utilsPage = new UtilsPage();
        contactUsPage = new ContactUsPage();
    }

    //
    @Given("^I am on the QAWorks \"([^\"]*)\"$")
    public void i_am_on_the_QAWorks_Site(String url) {
        homePage.visit(url);

    }

    @When("^I navigate to contact page$")
    public void iNavigateToContactPage() {
        contactUsPage.openContactUsPage();
    }

    @Then("^I should be able to contact QAWorks with the \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
    public void i_should_be_able_to_contact_QAWorks_with_the_following_information(String name, String mail, String text) {
        contactUsPage.fillContactUsForm(name, mail, text);
        contactUsPage.submit();
    }


}