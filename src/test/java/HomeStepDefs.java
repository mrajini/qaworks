import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by rajinim on 30/07/2017.
 */

public class HomeStepDefs {
    HomePage homePage;
    UtilsPage utilsPage;

    public HomeStepDefs() {
        homePage = new HomePage();
        utilsPage = new UtilsPage();
    }

    @Given("^I visited  \"([^\"]*)\"$")
    public void iVisited(String url) {
        homePage.visit(url);
    }

    @When("^I see the \"([^\"]*)\" of the Home Page$")
    public void iSeeTheOfTheHomePage(String arg0) {

        String aTitle = "";

        aTitle = homePage.getTitle();

        assertThat(aTitle, is("Software Quality and Delivery Experts"));
    }


    @Then("^I should see the \"([^\"]*)\" of the Home Page$")
    public void iShouldSeeTheOfTheHomePage(String arg0) {
        Assert.assertTrue(utilsPage.isElementPresent("#/contact.aspx", "css"));
    }


}

