import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by rajinim on 30/07/2017.
 */
public class ContactUsPage extends UtilsPage {

    @FindBy(css = "#ctl00_MainContent_NameBox")
    WebElement name;

    @FindBy(css = "#ContactEmailBox")
    WebElement email;

    @FindBy(css = "#ContactMessageBox")
    WebElement message;
    @FindBy(css = "#ContactSend")
    WebElement submitRequest;

    @FindBy(css = "a[href='/contact.aspx']")
    WebElement contact;


    public void openContactUsPage() {
        click(contact);
    }

    public void setName(String nme) {
        type(name, nme);
    }

    public void setEmail(String mail) {
        type(email, mail);
    }

    public void setMessage(String text) {
        type(message, text);
    }

    public void fillContactUsForm(String name, String mail, String info) {
        isElementPresent("#ctl00_MainContent_NameBox", "css");
        setName(info);
        isElementPresent("#ContactEmailBox", "css");
        setEmail(info);
        isElementPresent("#ContactMessageBox", "css");
        setMessage(info);
    }

    public void submit() {
        submitRequest.submit();
        isElementPresent("#ContactSend", "css");

    }
}
