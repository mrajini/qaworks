import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by rajinim on 30/07/2017.
 */
public class HomePage extends UtilsPage {


    WebDriver driver = BrowserFactory.getDriver();
    public String Url = "http://www.qaworks.com";

    @FindBy(id = "AboutUsHeaderDesc")
    WebElement title;

    @FindBy(css = "a[href='/contact.aspx']")
    WebElement contact;


    public String getTitle() {
        return title.getText();


    }
}
