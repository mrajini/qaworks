import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

/**
 * Created by rajinim on 30/07/2017.
 */
public class UtilsPage {

    private FluentWait<WebDriver> wait;


    /**
     * Initialise the page element and the fluent wait.
     */
    public UtilsPage() {
        PageFactory.initElements(BrowserFactory.getDriver(), this);
        wait = getWait();
    }

    /**
     * Returns the webdriver for current test session.
     *
     * @return
     */
    public WebDriver getDriver() {
        return BrowserFactory.getDriver();
    }

    /**
     * visit given url.
     *
     * @param url
     */
    public void visit(String url) {
        BrowserFactory.getDriver().get(url);

    }

    public String getCurrentUrl() {
        return getDriver().getCurrentUrl();
    }

    /**
     * Creating a standard fluent wait.
     *
     * @return
     */
    private FluentWait<WebDriver> getWait() {
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(getDriver())
                .withTimeout(120, TimeUnit.SECONDS)
                .pollingEvery(500, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .withMessage("ERROR: HAVE BEEN WAITING FOR TOO LONG - TIME OUT");

        return wait;
    }

    protected static WebDriver driver = BrowserFactory.getDriver();


    public static boolean isElementPresent(By element) {
        try {
            return driver.findElement(element).isDisplayed();

        } catch (Exception e) {
            return false;
        }
    }

    public static void sleep(int i) {
        try {
            Thread.sleep(i * 2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void selectFromDropDown(By by, String text) {
        Select sel = new Select(driver.findElement(by));
        sel.selectByVisibleText(text);
    }

    public static void selectFromDropDown1(By by, int index) {
        Select sel = new Select(driver.findElement(by));
        sel.selectByIndex(index);
    }

    public boolean isElementPresent(String s, String css) {
        //        waitForElementToBeVisible(element, locator);
        {
            return true;
        }
    }

    public void click(WebElement element) {
        try {
            element.click();

        } catch (Exception e) {
            System.out.printf("ERROR: Attempt to click on '%s' has failed", element.toString());
        }
    }

    public void type(WebElement element, String text) {
        try {
            element.clear();
            element.sendKeys(text);
        } catch (Exception e) {
            System.out.printf("ERROR: Attempting to type '%s' into '%s' ", text, element.toString());
        }
    }

//    private void waitForElementToBePresent(String element, String locator) {
//        wait.until(
//                new Predicate<WebDriver>() {
//                    public boolean apply(WebDriver driver) {
//                        List<WebElement> webElements = driver.findElements(Locators.valueOf(locator.toUpperCase())
//                                .getLocator(element));
//                        return webElements.size() > 0;
//                    }
//
//                }
//        );


}


